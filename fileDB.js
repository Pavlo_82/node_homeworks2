// imports
const fs = require('fs')
const fsp = require('fs/promises')
const path = require('path')
const nanoid = require('nanoid')

// errors
const errors = {
  notFound: {
    statusCode: 404,
    message: 'could not be found'
  },
  fileProblems: {
    statusCode: 500,
    message: 'cannot create file or directory'
  },
  field: {
    statusCode: 404,
    message: 'no field text or title'
  },
  database: {
    statusCode: 404,
    message: 'no database'
  }
}

// helper find elem
const findEl = (data, id) => {
  return data.find(item => item.id === id)
}

// validation
const validate = ({ text, title }) => {
  return { text, title }
}

const schemas = {}

const registerSchema = (schemaName, schema) => {
  schemas[schemaName] = schema
}

// function getTable return CRUD
const getTable = (database) => {
  // check table
  const schema = schemas[database]

  if (!schema) {
    throw new Error(errors.database)
  }

  // check if folder or create
  const folder = path.join(__dirname, 'db')
  const exists = fs.existsSync(folder)
  const file = path.join(folder, `${database}.json`)
  if (!exists) {
    fsp.mkdir(folder)
    // create file
    fsp.writeFile(file, JSON.stringify([]), (err, data) => {
      if (err) {
        return errors.fileProblems
      }
      return data
    })
  }

  // read
  const readData = async () => {
    const product = await fsp.readFile(file, 'utf8', (err, data) => {
      if (!err) {
        return data
      } else {
        return err
      }
    })
    return JSON.parse(product)
  }
  // write
  const writeData = async (data) => {
    fs.writeFile(file, JSON.stringify(data), (err) => {
      if (err) {
        return errors.fileProblems
      }
      return data
    })
  }
  return {
    // create
    async create (data) {
      const { title, text } = validate(data)
      if (!title || !text) {
        return errors.field
      }
      // generate id
      const id = nanoid.nanoid()
      // generate date
      const createDate = new Date()
      const formData = {
        id,
        title,
        text,
        createDate,
        updateDate: createDate
      }
      const all = await readData()
      const newData = [...all, formData]
      await writeData(newData)
      return formData
    },
    // update
    async update (data, id) {
      const { title, text } = validate(data)
      const all = await readData()
      const updateDate = new Date()
      const finded = findEl(all, id)
      if (!finded) {
        return errors.notFound
      }
      const updatedText = text ? text : finded.text
      const updatedTitle = title ? title : finded.title
      const updated = {
        id: finded.id,
        text: updatedText,
        title: updatedTitle,
        createDate: finded.createDate,
        updateDate
      }
      const newData = all.map((item) => item.id === id ? updated : item)
      await writeData(newData)
      return updated
    },
    // get by id
    async getById (id) {
      const all = await readData()
      const finded = findEl(all, id)
      if (!finded) {
        return errors.notFound
      }
      return finded
    },
    // get all
    async getAll () {
      return await readData()
    },
    // delete
    async delete (id) {
      const all = await readData()
      const finded = findEl(all, id)
      if (!finded) {
        return errors.notFound
      }
      const deleted = all.filter(item => item.id !== id)
      await writeData(deleted)
      return {
        message: { message: 'succesfull deleted', id }
      }
    }
  }
}

module.exports = {
  getTable,
  registerSchema
}
